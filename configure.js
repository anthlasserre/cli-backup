const inquirer = require("inquirer")

const questions = [
    {
        type: "rawlist",
        name: "method",
        message: "🚀    Select your transfert method:",
        choices: ["FTP", "SSH"]
    },
    {
        type: 'string',
        name: 'ip_address',
        message: '🔢    Enter the distant server IP Address:',
        validate: function(value) {
            const regex = /^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/
            if (regex.test(value)){
                return true
            } else {
                return 'Please enter a valid IP Address'
            }
        },
        filter: String
    },
    {
        type: 'string',
        name: 'user',
        message: '👨🏼‍    Enter the user:',
    },
    {
        type: 'password',
        name: 'password',
        message: '🔐    Enter the password:',
    },
    {
        type: 'string',
        name: 'backup_path',
        message: '📦   Enter the backup distant path directory',
    },
]

inquirer
  .prompt(questions)
  .then(answers => {
    // Use user feedback for... whatever!!
    console.log("ANSWERS", answers)
  });