const path = require("path")
const FTPClient = require("ftp")
const ora = require('ora');

// Import config
const config = require("../config.json")

// Envoie du fichier .backup sur le serveur distant
module.exports.SendFTPBackupFile = async (backupFilePath) => {
    const filename = path.basename(backupFilePath)

    // create a new progress bar instance and use shades_classic theme
    let spinner = ora('🚀  SENDING BACKUP BY FTP').start();


    try {
        const c = new FTPClient()

        c.on("ready", async () => {
            await c.put(backupFilePath, `${config.distant.backup_path}/${filename}`, (err) => {
                if (err){
                    console.error("CANNOT SEND BACKUP", err)
                    throw err
                }
                c.end()
            })
        })

        c.on("close", () => {
            spinner.succeed("🚀  BACKUP SUCCESSFULLY SEND BY FTP")
        })

        // connect to distant_ip:21 w/ config
        c.connect({
            host: config.distant.ip_address,
            user: config.distant.user,
            password: config.distant.password
        })
        return "BACKUP SENT BY FTP"
    } catch (error) {
        console.error("CANNOT SEND BACKUP", error)
    }
}