const fs = require("fs")

// Récupération des dossiers à sauvegarder
module.exports.CheckElementsToSave = async (elements) => {
    return elements.map(e => {
        try {
            if (fs.existsSync(e.path)) {
                // console.log("FILE OR DIRECTORY EXISTS", e.path)
                //file exists
                return e
            }
          } catch(err) {
            console.error("FILE OR DIRECTORY NOT EXISTS", e.path, err)
          }
    })
}