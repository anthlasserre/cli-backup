
const fs = require("fs")
const archiver = require("archiver")
const moment = require("moment")
const cliProgress = require('cli-progress');

// Création d'un fichier .backup avec tous les fichiers & dossiers listés
module.exports.CreateBackupFile = async (localBackupPath, elements) => {
    const now = new Date()
    const dateTime = moment(now).format('YYYYMMDD_HHmmss')
    const zipFilePath = `${localBackupPath}/cli-backup_${dateTime}.zip`

    // create a new progress bar instance and use shades_classic theme
    const loadingBar = new cliProgress.SingleBar({
        format:'📦  CREATING BACKUP [{bar}] {percentage}% | TIME: {duration}s | {value}/{total}'
    }, cliProgress.Presets.shades_classic);

    try {
        let output = fs.createWriteStream(zipFilePath)
        let archive = archiver("zip", {
            // zlib: { level: 9 } // Sets the compression level.
        })
        
        // good practice to catch warnings (ie stat failures and other non-blocking errors)
        archive.on("warning", (err) => {
            if (err.code === "ENOENT") {
                // log warning
                console.warn("WARNING ON ARCHIVE", err)
            } else {
                // throw error
                throw err
            }
        });
        
        // good practice to catch this error explicitly
        archive.on("error", (err) => {
            throw err
        })

        archive.on("progress", (progress) => {
            const { total, processed } = progress.entries
            if(processed <= 10){
                loadingBar.start(total, 0);
            }
            // console.log("TOTAL",total, "PROCESSED", processed)
            // update the current value in your application..
            loadingBar.setTotal(total)
            loadingBar.update(processed);
        })

        archive.on("finish", (finish) => {
            // stop the progress bar
            loadingBar.stop();
        })
        
        // pipe archive data to the file
        archive.pipe(output);

        elements.map(async e => {
            await archive.glob(
                "**/**", {
                    cwd: e.path,
                    ignore: e.excludes,
            }, {
                prefix: `cli-backup_${dateTime}/${e.name}`
            })
        })
        
        await archive.finalize()
        return zipFilePath
    } catch (error) {
        console.error("CANNOT CREATE BACKUP", error)
    }
}
