// Import functions
const { CheckElementsToSave } = require("./functions/checkElementsToSave")
const { CreateBackupFile } = require("./functions/createBackupFile")
const { SendFTPBackupFile } = require("./functions/sendFTPBackupFile")

// Import config
const config = require("./config.json")

// Build the main process
const MainProcess = async () => {
    const checkedElements = await CheckElementsToSave(config.repositories)
    const zipFilePath = await CreateBackupFile(config.local.backup_path, checkedElements)
    const sendedZipFile = await SendFTPBackupFile(zipFilePath)
}

// Run the main process
MainProcess()