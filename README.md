# CLI Backup
**Backup your files** easily with a simple node script via different ways like FTP, SSH and more...

## Roadmap 🚀
- [x] Generate Backup & Transfer via FTP

- [ ] Transfer via SSH

- [ ] Cron execution

- [ ] Top Bar Icon access

- [ ] npm package

# Install
> Before to start be sure to have **node** installed. If not, please [install it here](https://nodejs.org/en/download/).

Install dependencies with `$ npm install`

# Configure
Configure the script by running `$ npm run configure` or by editing **config.json**.

# Run
After configured this script to your need, please run the following command:

`$ npm run start`